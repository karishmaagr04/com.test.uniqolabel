package com.test.uniqolabel;

/*
 * @author Karishma Agrawal on 2019-05-27.
 */

import android.support.v4.app.Fragment;

import com.bumptech.glide.Glide;

public class ListViewFragment extends Fragment {


    private void loadDataFromGlide() {
        Glide
                .with(context)
                .load(BASE_URL_IMG + result.getPosterPath())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        // TODO: 08/11/16 handle failure
                        movieVH.mProgress.setVisibility(View.GONE);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        // image ready, hide progress now
                        movieVH.mProgress.setVisibility(View.GONE);
                        return false;   // return false if you want Glide to handle everything else.
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.ALL)   // cache both original & resized image
                .centerCrop()
                .crossFade()
                .into(movieVH.mPosterImg);
    }
}
