package com.test.uniqolabel;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
//        viewPagerAdapter.addFragment(new ListViewFragment(), "List View");
//        viewPagerAdapter.addFragment(new GridViewFragment(), "Grid View");
        getGameList();
    }


    private void getGameList() {

        new Runnable() {
            @Override
            public void run() {

                final Data[] newList = new Data[10];
                int comicNumber = 1;

                /*Create handle for the RetrofitInstance interface*/
                apiCall service = RetrofitClientInstance.getRetrofitInstance().create(apiCall.class);

                for (int i = 0; i < 10; i++) {
                    Call<Data> call = service.getDataList1(comicNumber);
                    comicNumber++;

                    call.enqueue(new Callback<Data>() {
                        @Override
                        public void onResponse(Call<Data> call, Response<Data> response) {
                            newList[i] = response.body();
                        }

                        @Override
                        public void onFailure(Call<Data> call, Throwable t) {
                            Toast.makeText(getApplicationContext(), "Something went wrong...Please try later!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

            }
        };


    }

}
