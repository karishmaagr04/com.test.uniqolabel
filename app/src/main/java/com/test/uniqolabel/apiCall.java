package com.test.uniqolabel;
/*
 * @author Karishma Agrawal on 2019-05-27.
 */

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface apiCall {

    @GET("{comicNumber}/info.0.json")
    Call<Data> getDataList1(@Path("comicNumber") int comicNumber);

}
